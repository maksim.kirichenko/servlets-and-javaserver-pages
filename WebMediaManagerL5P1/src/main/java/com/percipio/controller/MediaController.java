package com.percipio.controller;

import com.percipio.bean.FileMediaBean;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "MediaController", urlPatterns = {"/MediaController"})
public class MediaController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        processRequest(req, resp);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html;charset=UTF-8");
        ServletContext context = getServletContext();
        String realPath = context.getRealPath("fxmedia");
        FileMediaBean fmm = new FileMediaBean(realPath);
        fmm.loadData();
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MediaController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MediaController at >" + request.getContextPath());
            out.println(fmm.getPicturesCount() + " Pictures, " + fmm.getVideoCount());
//            for (Med)
            out.println("</body>");
        }
    }
}
